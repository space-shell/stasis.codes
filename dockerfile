FROM elixir:alpine as builder

RUN apk add --update git && \
    rm -rf /var/cache/apk/*


# Rust compiler required for the Rambo package to run on ARM
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

ENV PATH="/root/.cargo/bin:${PATH}"
# Rust...

ARG SECRET_KEY_BASE
ENV SECRET_KEY_BASE=$SECRET_KEY_BASE

ENV MIX_ENV=prod
ENV PORT=443
ENV APP_HOME /app

RUN mkdir $APP_HOME
WORKDIR $APP_HOME

RUN mix local.hex --force
RUN mix local.rebar --force

COPY config config
COPY priv priv
COPY lib lib
COPY mix.exs .
COPY mix.lock .

RUN mix deps.get --only $MIX_ENV
RUN mix deps.compile
RUN mix release

FROM alpine AS app

ENV LANG=C.UTF-8

RUN apk add --update openssl ncurses-libs postgresql-client libgcc libstdc++ && \
    rm -rf /var/cache/apk/*

RUN adduser -D -h /home/stasis stasis
WORKDIR /home/stasis
COPY --from=builder /app/_build .
RUN chown -R stasis: ./prod
USER stasis

EXPOSE 443

CMD ["./prod/rel/stasis/bin/stasis", "start"]
