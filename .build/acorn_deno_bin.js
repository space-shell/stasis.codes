import { readAll } from "https://deno.land/std@0.101.0/io/util.ts";
import { parse } from "https://cdn.skypack.dev/acorn";

const input = await readAll(Deno.stdin);

const input_text = new TextDecoder().decode(input);

const ast = parse(input_text, { ecmaVersion: "10", locations: false, locations: true });

const ast_string = JSON.stringify(ast, null, null);

console.log(ast_string);

Deno.exit(0);
