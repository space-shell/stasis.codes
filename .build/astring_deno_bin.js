import { generate } from "https://deno.land/x/astring/src/astring.js"
import { readAll } from "https://deno.land/std@0.101.0/io/util.ts";

const input = await readAll(Deno.stdin);

const input_text = new TextDecoder().decode(input);

const code = generate(JSON.parse(input_text), {
  indent: '  ',
  lindeEnd: '\n',
  startingIndentLevel: 0,
})

console.log(code);

Deno.exit(0);
