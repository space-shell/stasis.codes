defmodule Stasis.MixProject do
  use Mix.Project

  def project do
    [
      app: :stasis,
      version: "0.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Stasis.Application, []},
      extra_applications: [:logger, :runtime_tools, :crypto, :traverse, :base64_vlq]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.5.9"},
      {:phoenix_live_view, "~> 0.15.1"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_dashboard, "~> 0.4"},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:httpoison, "~> 1.8"},
      {:ok, "~> 2.3"},
      {:traverse, "~> 1.0.1"},
      {:rambo, "~> 0.3.4"},
      {:typed_struct, "~> 0.2.1"},
      {:absinthe, "~> 1.5"},
      {:absinthe_phoenix, "~> 2.0"},
      {:elixir_uuid, "~> 1.2"},
      {:base64_vlq, github: "bryanjos/base64_vlq", branch: "master"},
      {:estree, github: "elixirscript/elixir-estree", tag: "v2.6.1"}
      # {:wasmex, "~> 0.5.0"},
    ]
  end

  defp aliases do
    [
      setup: ["deps.get"]
    ]
  end
end
