# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
import Config

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :stasis, StasisWeb.Endpoint,
  url: [host: "replik.io", port: 443],
  force_ssl: [hsts: true],
  secret_key_base: secret_key_base,
  server: true,
  https: [
    port: 443,
    cipher_suite: :strong,
    keyfile: System.get_env("KEY_PATH"),
    certfile: System.get_env("CERT_PATH"),
    transport_options: [socket_opts: [:inet6]]
  ]
