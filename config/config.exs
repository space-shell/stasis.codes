# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :stasis, StasisWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "RfyH0dnqv820bmAH/MjVrEZYlLLmMZuhBPrDjrPdPbXU9d9oB1mTCkDM3woKz3bn",
  render_errors: [view: StasisWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Stasis.PubSub,
  live_view: [signing_salt: "6bu/WaZP"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
