defmodule StasisWeb.Plug.ProjectContext do
  @behaviour Plug

  alias Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    case Conn.get_req_header(conn, "graphiql_project") do
      [project] ->
        Absinthe.Plug.put_options(conn, context: %{project: project})

      _ ->
        conn
    end
  end
end
