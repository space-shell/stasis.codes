defmodule StasisWeb.Content.Configure do
  use StasisWeb, :live_component

  @impl true
  def mount(socket) do
    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~L"""
      <div>
        Here is some config information
      </div>
    """
  end
end
