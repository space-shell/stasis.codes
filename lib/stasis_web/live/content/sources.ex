defmodule StasisWeb.Content.Sources do
  use StasisWeb, :live_view

  alias Stasis.Utils.GraphQuery, as: Gq

  @impl true
  def mount(_params, %{"project" => project}, socket) do
    run = Gq.runner(project)

    {:ok, %{"subscribed" => topic}} =
      run.("""
      subscription {
        sourceUrl
      }
      """)

    {:ok, %{data: data}} =
      run.("""
      query {
        project {
          source {
            url
          }
        }
      }
      """)

    :ok = StasisWeb.Endpoint.subscribe(topic)

    {:ok,
     assign(
       socket,
       source_url: nil,
       source: get_in(data, ["project", "source", "url"]),
       project: project
     )}
  end

  @impl true
  def handle_event("source-update", %{"source-url" => url}, socket) do
    case validate_uri(url) do
      {:ok, _} ->
        {:noreply, assign(socket, source_url: url)}

      {:error, _} ->
        {:noreply, assign(socket, source_url: nil)}
    end
  end

  @impl true
  def handle_event("source-add", %{"source-url" => sourceUrl}, socket) do
    {:ok, %{data: %{"sourceUrl" => url}}} =
      Gq.run(
        """
        mutation {
          sourceUrl(url: #{if is_nil(sourceUrl), do: nil, else: "\"#{sourceUrl}\""})
        }
        """,
        socket.assigns.project
      )

    socket
    |> assign(source: url, source_url: nil)
    |> (&{:noreply, &1}).()
  end

  @impl true
  def handle_info(
        %Phoenix.Socket.Broadcast{
          event: "subscription:data",
          payload: %{
            result: %{data: data}
          }
        },
        socket
      ) do
    case data do
      %{"sourceUrl" => sourceUrl} ->
        {:noreply, assign(socket, source: sourceUrl, source_url: nil)}

      data_ ->
        IO.inspect(data_, label: "RANDOM SOURCE INFO")

        {:noreply, socket}
    end
  end

  defp validate_uri(url) do
    case URI.parse(url) do
      %URI{scheme: nil} -> {:error, url}
      %URI{host: nil} -> {:error, url}
      %URI{host: ""} -> {:error, url}
      %URI{path: nil} -> {:error, url}
      uri -> {:ok, uri}
    end
  end

  @impl true
  def render(assigns) do
    ~L"""
      <form
        class="flex flex-row gap-1"
        phx-submit="source-add"
        phx-change="source-update" >

        <input
          class="<%= if is_nil( @source ), do: "text-black bg-red-100", else: "bg-black text-red-100" %> focus:outline-none block border-black border-b-4 h-16 w-full text-center"
          placeholder="source url"
          value="https://replik.io/js/test.js"
          name="source-url"
          autocomplete="off"
          type="url"
          <%= if not is_nil( @source ), do: "disabled value=#{@source}" %> >

        <br />

        <button
          type="submit"
          class="<%= if is_nil( @source_url ) and is_nil(@source), do: "text-black bg-red-100", else: "bg-black text-red-100" %> transition h-16 w-16 mx-auto"
          <%= if is_nil( @source_url ) and is_nil(@source), do: "disabled" %> >
          <strong
            class="<%= if not is_nil( @source ), do: "rotate-45" %> transition-transform transform inline-block text-2xl" >
            +
          </strong>
        </button>
      </form>
    """
  end
end
