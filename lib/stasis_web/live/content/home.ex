defmodule StasisWeb.Content.Home do
  use StasisWeb, :live_component

  @impl true
  def mount(socket) do
    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~L"""
      <div class="h-full flex justify-center items-center">
        <h1 class="p-8 bg-black text-red-100 uppercase tracking-widest">
          Home
        </h1>
      </div>
    """
  end
end
