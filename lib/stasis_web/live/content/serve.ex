defmodule StasisWeb.Content.Serve do
  use StasisWeb, :live_view

  # alias Stasis.Utils.GraphQuery, as: Gq

  @impl true
  def mount(_params, %{"project" => project}, socket) do
    # run = Gq.runner(project)

    {:ok, assign(socket, project: project)}
  end

  @impl true
  def handle_event("modify", _args, socket) do
    {:noreply, socket}
  end

  def html_url(project) do
    "http://jn-server.zero:4000/#{project}/html"
  end

  @impl true
  def render(assigns) do
    ~L"""
      <div class="flex-1 flex flex-col gap-4 items-center justify-center">
        <iframe
          width="100%"
          height="100%"
          class="border-4 border-solid border-black"
          src="<%= html_url(@project) %>" />
      </div>
    """
  end
end
