defmodule StasisWeb.Content.React do
  use StasisWeb, :live_view

  alias Stasis.Utils.GraphQuery, as: Gq

  @impl true
  def mount(_params, %{"project" => project}, socket) do
    run = Gq.runner(project)

    {:ok, %{"subscribed" => topic}} =
      run.("""
      subscription {
        components {
          name
          transforms {
            id
          }
        }
      }
      """)

    {:ok, %{data: data}} =
      run.("""
      query {
        project {
          source {
            file {
              ast {
                components {
                  name
                }
              }
            }
          }
        }
      }
      """)

    :ok = Gq.subscribe(topic)

    components =
      data
      |> get_in(["project", "source", "file", "ast", "components"])
      |> (&(&1 || [])).()
      |> Enum.map(&Map.get(&1, "name"))

    {:ok,
     assign(socket,
       project: project,
       modify: nil,
       component: nil,
       components: components,
       transform: nil,
       transforms: nil
     )}
  end

  @impl true
  def handle_event("component-select", %{"component" => component}, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        query {
          project {
            source {
              file {
                ast {
                  component(name: "#{component}") {
                    transforms {
                      id
                      active
                      condition {
                        type
                        key
                        value
                      }
                      alteration {
                        type
                        key
                        value
                      }
                    }
                  }
                }
              }
            }
          }
        }
        """,
        socket.assigns.project
      )

    transforms =
      data
      |> tap(&IO.inspect/1)
      |> get_in(["project", "source", "file", "ast", "component", "transforms"])

    {:noreply,
     assign(
       socket,
       component: component,
       transforms: transforms,
       transform: nil
     )}
  end

  @impl true
  def handle_event(
        "transform-select",
        %{"component" => component, "transform" => transform},
        socket
      ) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        query {
          project {
            source {
              file {
                ast {
                  component(name: "#{component}") {
                    transform(id: "#{transform}") {
                      id
                      active
                      condition {
                        type
                        key
                        value
                      }
                      alteration {
                        type
                        key
                        value
                      }
                    }
                  }
                }
              }
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply,
     assign(socket,
       transform: get_in(data, ["project", "source", "file", "ast", "component", "transform"])
     )}
  end

  @impl true
  def handle_event("transform-add", %{"component" => component}, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        mutation {
          transformAdd(
            category: "component",
            name: "#{component}"
          ) {
            id
            active
            condition {
              type
              key
              value
            }
            alteration {
              type
              key
              value
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply,
     assign(socket,
       transform: get_in(data, ["transformAdd"])
     )}
  end

  @impl true
  def handle_event("modify", %{"value" => modify}, socket) do
    cond do
      bit_size(modify) === 0 ->
        {:noreply, assign(socket, :modify, nil)}

      modify === Atom.to_string(socket.assigns.modify) ->
        {:noreply, assign(socket, :modify, nil)}

      true ->
        case modify do
          "conditions" ->
            {:noreply, assign(socket, :modify, :conditions)}

          "alterations" ->
            {:noreply, assign(socket, :modify, :alterations)}
        end
    end
  end

  @impl true
  def handle_event("condition-change", condition, socket) do
    {:noreply,
     update(
       socket,
       :transform,
       fn transform ->
         condition
         |> Map.drop(["_target"])
         |> (fn alt ->
               Map.update(
                 transform,
                 "condition",
                 nil,
                 &Map.merge(alt, &1 || %{})
               )
             end).()
       end
     )}
  end

  @impl true
  def handle_event("condition-update", %{"type" => type, "key" => key, "value" => value}, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        mutation {
          transformConditionUpdate(
            category: "component"
            name: "#{socket.assigns.component}"
            id: "#{socket.assigns.transform["id"]}"
            type: "#{type}"
            key: "#{key}"
            value: "#{value}"
          ) {
            id
            active
            condition {
              type
              key
              value
            }
            alteration {
              type
              key
              value
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply, assign(socket, :transform, data["transformConditionUpdate"])}
  end

  @impl true
  def handle_event("alteration-change", alteration, socket) do
    {:noreply,
     update(
       socket,
       :transform,
       fn transform ->
         alteration
         |> Map.drop(["_target"])
         |> (fn alt ->
               Map.update(
                 transform,
                 "alteration",
                 nil,
                 &Map.merge(alt, &1 || %{})
               )
             end).()
       end
     )}
  end

  @impl true
  def handle_event("alteration-update", %{"type" => type, "key" => key, "value" => value}, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        mutation {
          transformAlterationUpdate(
            category: "component"
            name: "#{socket.assigns.component}"
            id: "#{socket.assigns.transform["id"]}"
            type: "#{type}"
            key: "#{key}"
            value: "#{value}"
          ) {
            id
            active
            condition {
              type
              key
              value
            }
            alteration {
              type
              key
              value
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply, assign(socket, :transform, data["transformAlterationUpdate"])}
  end

  @impl true
  def handle_event("transform-active", _, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        mutation {
          transformActiveToggle(
            category: "component"
            name: "#{socket.assigns.component}"
            id: "#{socket.assigns.transform["id"]}"
          ) {
            id
            active
            condition {
              type
              key
              value
            }
            alteration {
              type
              key
              value
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply, assign(socket, :transform, data["transformActiveToggle"])}
  end

  @impl true
  def handle_event("transform-delete", _, socket) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        mutation {
          transformDelete(
            category: "component"
            name: "#{socket.assigns.component}"
            id: "#{socket.assigns.transform["id"]}"
          ) {
            id
            active
            condition {
              type
              key
              value
            }
            alteration {
              type
              key
              value
            }
          }
        }
        """,
        socket.assigns.project
      )

    {:noreply, assign(socket, :transform, data["transformDelete"])}
  end

  @impl true
  def render(assigns) do
    ~L"""
      <div class="flex-1 flex flex-col gap-4 items-center justify-center">
        <%= if not is_nil(@component) and is_nil(@transform) do %>
          <button
            class="h-16 w-16 bg-black text-red-100"
            phx-click="transform-add"
            phx-value-component="<%= @component %>"
            type="button" >
            <strong>
              +
            </strong>
          </button>
        <% end %>

        <%= if not is_nil(@transform) do %>
          <div class="w-full flex flex-row text-center">
            <button
              phx-click="modify"
              value="conditions"
              class="<%= if @modify === :conditions, do: "border-black", else: "border-red-100" %> border-b-4 border-solid flex-1 py-4 uppercase tracking-widest box-border">
              Conditions
            </button>
            <button
              phx-click="modify"
              type="button"
              class="<%= if @transform["active"], do: "bg-black text-red-100 border-4", else: "border-4 bg-red-100 text-black" %> p-4 uppercase tracking-widest border-solid border-black" >
              <%= @component %>
            </button>
            <button
              phx-click="modify"
              value="alterations"
              class="<%= if @modify === :alterations, do: "border-black", else: "border-red-100" %> border-b-4 border-solid flex-1 py-4 uppercase tracking-widest box-border">
              Alterations
            </button>
          </div>

          <%= if is_nil(@modify) do %>
            <button
              phx-click="transform-active"
              class="<%= if @transform["active"], do: "bg-black text-red-100", else: "border-4 border-solid border-black bg-red-100 text-black" %> h-24 w-24 my-auto"
              type="button" >

              <span class="text-center w-full inline-block transform -rotate-45 uppercase tracking-widest overflow-hidden">
              <%= if @transform["active"], do: "Active", else: "Inactive" %>
              </span>
            </button>

            <button
              phx-click="transform-delete"
              class="h-16 w-full hover:bg-black hover:text-red-100 bg-red-100 text-black"
              type="button" >

              <strong>
                x
              </strong>
            </button>
          <% end %>

          <%= if @modify === :conditions do %>
            <form
              phx-change="condition-change"
              phx-submit="condition-update"
              class="w-full flex flex-col gap-4 text-center my-auto max-w-lg">

              <select
                name="type"
                class="h-16 bg-red-100 border-b-4 border-solid border-black">

                <option
                  <%= if is_nil(@transform["condition"]["type"]), do: "selected" %>
                  disabled>

                  Condition
                </option>

                <option
                  <%= if @transform["condition"]["type"] === "render", do: "selected" %>
                  value="<%= :component %>">

                  Component
                </option>
              </select>

              <%= if not is_nil( @transform["condition"] ) do %>
                <%= if @transform["condition"]["type"] === "component" do %>
                  <input
                    name="key"
                    value="<%= @component %>"
                    class="h-16 bg-red-100 border-b-4 border-solid border-black pointer-events-none">

                  <input type="hidden" name="value" value="<%= nil %>">
                <% end %>

                <button
                  class="h-16 w-16 mx-auto bg-black text-red-100"
                  type="submit" >
                  <strong>
                    +
                  </strong>
                </button>
              <% end %>
            </form>
          <% end %>

          <%= if @modify === :alterations do %>
            <form
              phx-change="alteration-change"
              phx-submit="alteration-update"
              class="w-full flex flex-col gap-4 text-center my-auto max-w-lg">

              <select
                name="type"
                class="h-16 bg-red-100 border-b-4 border-solid border-black">

                <option
                  <%= if is_nil(@transform["alteration"]["type"]), do: "selected" %>
                  disabled>

                  Alteration
                </option>

                <option
                  <%= if @transform["alteration"]["type"] === "render", do: "selected" %>
                  value="<%= :render %>">

                  Render
                </option>

                <!--
                <option value="<%= :props %>">
                  Props
                </option>

                <option value="<%= :change %>">
                  Replace
                </option>
                -->
              </select>

              <%= if not is_nil( @transform["alteration"] ) do %>
                <%= if @transform["alteration"]["type"] === "render" do %>
                  <input type="hidden" name="key" value="value">

                  <select
                    name="value"
                    class="h-16 bg-red-100 border-b-4 border-solid border-black">

                    <%= for bool <- ["true", "false"] do %>
                      <option
                        <%= if @transform["alteration"]["value"] === bool, do: "selected" %>
                        value="<%= bool %>">

                        <%= bool %>
                      </option>
                    <% end %>
                  </select>
                <% end %>

                <button
                  class="h-16 w-16 mx-auto bg-black text-red-100"
                  type="submit" >
                  <strong>
                    +
                  </strong>
                </button>
              <% end %>
            </form>
          <% end %>

          <%= if false do %>
            <div>
              <button
                class="h-16 w-16 bg-black text-red-100"
                type="button" >
                <strong>
                  AND
                </strong>
              </button>

              <button
                class="h-16 w-16 bg-black text-red-100"
                type="button" >
                <strong>
                  OR
                </strong>
              </button>
            </div>
          <% end %>
        <% end %>
      </div>

      <vr class="block h-full border-solid border-r-4 border-black mx-4" >
      </vr>

      <div class="flex flex-col">
        <span class="tracking-widest uppercase">
          Components
        </span>

        <hr class="border-solid border-b-4 border-t-0 border-black my-4" />

        <ul class="w-64 overflow-auto">
          <%= for component <- @components do %>
            <li>
              <button
                class="<%= if component === @component, do: "bg-black text-red-100" %> w-full text-left hover:bg-black hover:text-red-100 py-1 px-2"
                type="button"
                phx-value-component="<%= component %>"
                phx-click="component-select">
                <%= component %>
              </button>
              <%= if component === @component and not is_nil(@transforms) do %>
                <ul>
                  <%= for {%{ "id" => id }, alias} <- Enum.zip(@transforms, ["alpha", "bravo", "charlie", "delta"]) do %>
                    <li>
                      <button
                        class="<%= if id === get_in(@transform, ["id"]), do: "bg-black text-red-100" %> border-black border-l-4 border-solid ml-2 w-full text-left hover:bg-black hover:text-red-100 py-1 px-2 uppercase tracking-widest"
                        type="button"
                        phx-value-component="<%= component %>"
                        phx-value-transform="<%= id %>"
                        phx-click="transform-select">
                        <%= alias %>
                      </button>
                    </li>
                  <% end %>
                </ul>
              <% end %>
            </li>
          <% end %>
        </ul>
      </div>
    """
  end
end
