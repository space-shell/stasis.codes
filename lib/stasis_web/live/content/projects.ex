defmodule StasisWeb.Content.Projects do
  use StasisWeb, :live_view

  alias Stasis.Utils.GraphQuery, as: Gq

  @impl true
  def mount(_params, %{"project" => project}, socket) do
    run = Gq.runner(project)

    {:ok, %{"subscribed" => topic}} =
      run.("""
      subscription {
        projects
      }
      """)

    {:ok, %{data: %{"projects" => projects}}} =
      run.("""
      query {
        projects {
          name
        }
      }
      """)

    :ok = Gq.subscribe(topic)

    projects
    |> Enum.map(&{Map.get(&1, "name"), true})
    |> Map.new()
    |> (&{:ok, assign(socket, projects: &1)}).()
  end

  @impl true
  def handle_event("project-select", %{"value" => name}, socket) do
    # TODO - Check of empty string

    {:noreply, push_redirect(socket, to: "/" <> name <> "/configure")}
  end

  @impl true
  def handle_event("project-add", %{"project-name" => name}, socket) do
    if name |> String.trim() |> String.length() |> Kernel.===(0) do
      {:noreply, socket}
    else
      {:ok, _pid} = Stasis.Project.Dynamic.create(name)

      {:noreply, update(socket, :projects, &Map.put(&1, name, false))}
    end
  end

  @impl true
  def handle_info({:project, name}, socket) do
    {:noreply, update(socket, :projects, &%{&1 | name => true})}
  end

  @impl true
  def render(assigns) do
    ~L"""
      <form
        phx-submit="project-add" >

        <input
          class="h-16 border-b-4 mb-1 transition bg-red-100 focus:outline-none block border-black max-w-md mx-auto w-full text-center" placeholder="new project"
          autocomplete="off"
          name="project-name"
          type="text" />

        <br />

        <button
          class="bg-black text-red-100 block h-12 w-12 mx-auto transition-all"
          type="submit">

          <strong
            class="inline-block transform transition-transform text-2xl h-full align-text-top">
            +
          </strong>
        </button>
      </form>

      <ul class="h-full overflow-y-auto">
        <%= for {project, valid} <- @projects do %>
        <li>
          <button
            phx-click="project-select"
            value="<%= if valid, do: project, else: nil %>"
            type="button"
            class="<%= if valid, do: "hover:bg-black hover:text-white", else: "hover:border-black cursor-default" %> border-4 border-transparent box-border w-full duration-300 p-4 text-center transition-all uppercase tracking-widest">
            <%= project %>
          </button>
        </li>
        <% end %>
      </ul>
    """
  end
end
