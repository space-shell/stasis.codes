defmodule StasisWeb.ActionConfigure do
  use StasisWeb, :live_view

  @impl true
  def mount(_params, %{"project" => project}, socket) do
    if is_nil(project) do
      []
    else
      [
        {"Serve", Routes.page_path(socket, :serve, project)},
        {"React", Routes.page_path(socket, :react, project)},
        {"Sources", Routes.page_path(socket, :sources, project)}
      ]
    end
    |> Enum.concat([{"Projects", Routes.page_path(socket, :projects)}])
    |> Enum.reverse()
    |> (&{:ok, assign(socket, capabilities: &1, project: project)}).()
  end

  @impl true
  def render(assigns) do
    ~L"""
      <div class="flex flex-row gap-1">
        <div class="flex-1">
        </div>
        <%= for { name, to } <- @capabilities do %>
          <div
            class="w-24 h-24 py-8 bg-red-100 relative">
            <%= live_patch(name, to: to, class: "text-center w-full inline-block transform -rotate-45 uppercase tracking-widest overflow-hidden") %>
          </div>
        <% end %>
        <div class="flex-1">
        </div>
      </div>
    """
  end
end
