defmodule StasisWeb.PageLive do
  use StasisWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, project: nil)}
  end

  @impl true
  def handle_event("select-project", %{"value" => project}, socket) do
    {:noreply, assign(socket, :project, project)}
  end

  @impl true
  def handle_params(%{"project" => name}, _uri, socket) do
    socket
    |> assign(:project, name)
    |> (&{:noreply, &1}).()
  end

  @impl true
  def handle_params(_params, _route, socket) do
    {:noreply, socket}
  end
end
