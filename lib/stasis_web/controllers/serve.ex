defmodule StasisWeb.Controllers.Serve do
  use StasisWeb, :controller
  use Phoenix.HTML

  alias Stasis.Utils.RegistrationName, as: Reg

  defp html_template(assigns) do
    ~E"""
    <head>
      <script src="https://megaweb-media.ao.com/ao.web.aoreact/16/aoreact.bundle.js">
      </script>

      <script src="/<%= @project %>/js">
      </script>
    </head>
    <body>
      <app>
      </app>
      <script>
        window.BuyAnywhere.render({"priceIncVat":59.0,"isAvailable":true,"hasAttachment":false,"language":"en","catalogueItemId":44109,"clientId":1,"categoryIds":[250,260],"courierIds":[21,22],"addToBasketUrl":"https://checkout.ao.com/HttpHandlers/AddToBasketHandler.ashx?action=ADD-SUMMARY&ProductCode=QQ2-00012&timestamp=1633013088&h=925718BCAE6EE815BC29FA52AE057CED43861DB6E802E4C246F569E7066ECB8D","productType":"Digital","buttons":[{"containerSelector":"app","className":"cta cta-lg inline-block min-w-full"}]})
      </script>
    </body>
    """
  end

  def javascript(conn, %{"project" => project}) do
    case GenServer.whereis(Reg.via(project, Stasis.Project.Supervisor)) do
      nil ->
        conn
        |> send_resp(422, "")

      _ ->
        session = Plug.Conn.get_session(conn, :session_id)

        conn
        |> put_resp_content_type("application/javascript")
        |> send_resp(200, Stasis.Project.Serve.generate(project, session))
    end
  end

  def html(conn, %{"project" => project}) do
    case GenServer.whereis(Reg.via(project, Stasis.Project.Supervisor)) do
      nil ->
        conn
        |> send_resp(422, "")

      _ ->
        # session = Plug.Conn.get_session(conn, :session_id)

        html =
          %{project: project}
          |> html_template()
          |> html_escape()
          |> safe_to_string()

        conn
        |> put_resp_content_type("text/html")
        |> send_resp(200, html)
    end
  end
end
