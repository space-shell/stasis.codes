defmodule StasisWeb.Router do
  use StasisWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {StasisWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :serve do
    plug :accepts, ["javascript"]
    plug :fetch_session
    plug StasisWeb.Plug.SessionId
  end

  pipeline :graphiql do
    plug StasisWeb.Plug.ProjectContext
  end

  scope "/", StasisWeb do
    pipe_through :browser

    live "/", PageLive, :home
    live "/about", PageLive, :about
    live "/contact", PageLive, :contact
    live "/projects", PageLive, :projects
    live "/:project/configure", PageLive, :configure
    live "/:project/configure/sources", PageLive, :sources
    live "/:project/configure/react", PageLive, :react
    live "/:project/serve", PageLive, :serve
  end

  scope "/", StasisWeb do
    pipe_through :serve

    get "/:project/js", Controllers.Serve, :javascript
    get "/:project/html", Controllers.Serve, :html
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :graphiql

      forward "/graphiql", Absinthe.Plug.GraphiQL, schema: Stasis.Graph.Schema, socket: StasisWeb.UserSocket
    end

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: StasisWeb.Telemetry
    end
  end
end
