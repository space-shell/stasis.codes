defmodule Stasis.Project.Supervisor do
  use Supervisor
  alias Stasis.Utils.RegistrationName, as: Reg

  def start_link(project: project) do
    {:ok, _pid} =
      Supervisor.start_link(
        __MODULE__,
        [{:project, project}],
        [Reg.name(project, __MODULE__)]
      )
  end

  @impl true
  def init(project: project) do
    children = [
      {Stasis.Project.SourceFile, project: project},
      {Stasis.Project.SourceMap, project: project},
      {Stasis.Project.Transforms, project: project},
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
