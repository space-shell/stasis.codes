defmodule Stasis.Project.SourceMap do
  use GenServer
  use TypedStruct
  use Stasis.Using.Subscribable, __MODULE__
  use Stasis.Using.GetSetKey, __MODULE__

  alias Stasis.Utils.RegistrationName, as: Reg

  typedstruct do
    @typedoc "Source Map"

    field(:file, String.t())
    field(:mappings, [String.t()])
    field(:names, [String.t()])
    field(:sources, [String.t()])
    field(:sourcesContent, String.t())
    field(:version, integer())
  end

  def init(_args) do
    {:ok, url: nil, map: nil, mappings: nil}
  end

  def start_link(project: project) do
    GenServer.start_link(__MODULE__, [], [Reg.name(project, __MODULE__)])
  end

  def handle_call({:url, url, project}, _from, state) do
    {
      :reply,
      :ok,
      Keyword.replace(state, :url, url),
      {:continue, {:map, url, project}}
    }
  end

  def handle_call(:url, _from, state) do
    {:reply, Keyword.get(state, :url, nil), state}
  end

  def handle_continue(
        {:mappings,
         %__MODULE__{
           :mappings => mappings,
           :sources => sources
         }, _project},
        state
      ) do
    mappings
    |> String.split(";")
    |> Enum.map(fn
      "" ->
        nil

      vlq ->
        vlq
        |> String.split(",")
        |> Enum.map(&map_array/1)
    end)
    |> (&Enum.zip(sources, &1)).()
    |> (&{:noreply, Keyword.put(state, :mappings, &1)}).()
  end

  def handle_continue({:broadcast, url, mapping, project}, state) do
      broadcast(project, {:map_url, url})

      {:noreply, state, {:continue, {:mappings, mapping, project}}}
  end

  def handle_continue({:map, url, project}, state) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}} ->
        headers
        |> Enum.map(&elem(&1, 1))
        |> Enum.any?(&Enum.member?(["application/json", "application/octet-stream"], &1))
        |> if do
          {:ok, json} = body |> Jason.decode(keys: :atoms)

          body_ = struct(__MODULE__, json)

          state
          |> Keyword.put(:map, body_)
          |> (&{:noreply, &1, {:continue, {:broadcast, url, body_, project}}}).()
        else
          {:noreply, state}
        end

      {:ok, %HTTPoison.Response{status_code: status, body: _}} when status !== 200 ->
        IO.inspect(status, label: "Source Map Status")

        {:noreply, state}

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect(reason, label: "Source Map Error")

        {:noreply, state}
    end
  end

  defp map_array(vlq, arr \\ []) do
    if vlq === "" do
      arr
    else
      {:ok, %{:rest => rest, :value => value}} = Base64VLQ.decode(vlq, 0)

      map_array(String.slice(vlq, rest..16), arr ++ [value])
    end
  end

  def url(project, url) do
    case GenServer.whereis(Reg.via(project, __MODULE__)) do
      nil -> nil
      pid -> GenServer.call(pid, {:url, url, project})
    end
  end

  def url(project) do
    case GenServer.whereis(Reg.via(project, __MODULE__)) do
      nil -> nil
      pid -> GenServer.call(pid, :url)
    end
  end
end
