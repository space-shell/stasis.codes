defmodule Stasis.Project.SourceFile do
  use GenServer
  use Stasis.Using.GetSetKey, __MODULE__

  alias Stasis.Utils.RegistrationName, as: Reg

  @impl true
  def init(_args) do
    { :ok, [ast: nil, url: nil, source: nil] }
  end

  def start_link(project: project) do
    GenServer.start_link(__MODULE__, [], [Reg.name(project, __MODULE__)])
  end

  @impl true
  def handle_continue({:ast, source}, state) do
    {:ok, %Rambo{status: _status, out: ast, err: _err}} =
      Rambo.run(Application.app_dir(:stasis, "priv/ports/acorn"), in: source)

    state
    |> Keyword.put(:ast, ast)
    |> (&{:noreply, &1}).()
  end

  def handle_call({:source, url}, _from, state) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}} ->
        headers
        |> Enum.map(&elem(&1, 1))
        |> Enum.member?("application/javascript")
        |> if do
          state
          |> Keyword.put(:source, body)
          |> (&{:reply, {:ok}, &1, {:continue, {:ast, body}}}).()
        else
          {:reply, {:ok}, state}
        end

      {:ok, %HTTPoison.Response{status_code: status, body: _}} when status !== 200 ->
        IO.inspect(status, label: "Source Status")

        {:reply, {:error}, state}

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect(reason, label: "Source Error")

        {:reply, {:error}, state}
    end
  end

  def get_source_file(url, project) do
    GenServer.call(Reg.via(project, __MODULE__), {:source, url})
  end

  # defp source_map_url(source) do
  #   ~r/\/\/[#@]\s(source(?:Mapping)?URL)=\s*(\S+)/
  #   |> Regex.run(source)
  #   |> List.last()
  # end
end
