defmodule Stasis.Project.Dynamic do
  use DynamicSupervisor

  def start_link(_args) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def create(project) do
    DynamicSupervisor.start_child(
      __MODULE__,
      {Stasis.Project.Supervisor, project: project}
    )
  end

  def projects() do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.flat_map(fn {_, pid, _, _} ->
      Registry.keys(Stasis.Registry, pid)
    end)
    |> Enum.map(&String.split(&1, ":"))
    |> Enum.filter(&Enum.member?(&1, "project"))
    |> Enum.map(&hd/1)
  end

  def project(name) do
    projects()
    |> Enum.filter(fn project -> project === name end)
    |> case do
      [ project ] -> project
      [] -> nil
    end
  end
end
