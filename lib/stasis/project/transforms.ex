defmodule Stasis.Project.Transforms do
  use Agent
  use TypedStruct

  alias Stasis.Utils.RegistrationName, as: Reg

  alias __MODULE__, as: Ts

  typedstruct do
    @typedoc "AST Transform"

    field(:id, :string, default: "null")
    field(:active, boolean(), default: false)

    field(:condition, %Stasis.Transform.Condition{})

    field(:alteration, %Stasis.Transform.Alteration{})
  end

  def start_link(project: project) do
    Agent.start_link(fn -> Map.new() end, [Reg.name(project, __MODULE__)])
  end

  def key(segs) do
    segs
    |> Enum.reject(&is_nil/1)
    |> Enum.map(&String.downcase/1)
    |> Enum.join(".")
  end

  def create(project, category, name) do
    transform = %Ts{
      id: Integer.to_string(:erlang.system_time())
    }

    :ok =
      Agent.update(Reg.via(project, __MODULE__), fn transforms ->
        Map.put(
          transforms,
          key([category, name, transform.id]),
          transform
        )
      end)

    {:ok, transform}
  end

  def read(project, category \\ nil, name \\ nil, uuid \\ nil) do
    Agent.get(Reg.via(project, __MODULE__), &Map.keys/1)
    # FIXME - How does a nil value make it into the agent keys ?
    |> Enum.reject(&is_nil/1)
    |> Enum.filter(&String.contains?(&1, key([category, name, uuid])))
    |> (fn keys ->
          Agent.get(
            Reg.via(project, __MODULE__),
            &(Map.take(&1, keys) |> Map.values())
          )
        end).()
    |> case do
      [] ->
        {:ok, nil}

      [transform] ->
        {:ok, transform}

      transforms ->
        {:ok, transforms}
    end
  end

  def update(project, category, name, id, update) do
    :ok =
      Agent.update(Reg.via(project, __MODULE__), fn transforms ->
        Map.update(
          transforms,
          key([category, name, id]),
          nil,
          &update.(&1)
        )
      end)

    read(project, category, name, id)
  end

  def delete(project, category, name, id) do
    :ok =
      Agent.update(Reg.via(project, __MODULE__), fn transforms ->
        Map.drop(
          transforms,
          [key([category, name, id])]
        )
      end)

    read(project, category, name, id)
  end
end
