defmodule Stasis.Project.Serve do
  @moduledoc """
  Aggregates any available transformers with an available AST to produce an altered JavaScript file.
  """

  alias Stasis.Utils.GraphQuery, as: Gq

  def generate(project, _session) do
    {:ok, %{data: data}} =
      Gq.run(
        """
        query {
          project {
            source {
              file {
                ast {
                  tree
                  components {
                    transforms {
                      id
                      active
                      transformed
                      condition {
                        type
                        key
                        value
                      }
                      alteration {
                        type
                        key
                        value
                      }
                    }
                  }
                }
              }
            }
          }
        }
        """,
        project
      )

    transforms_ =
      Enum.flat_map(
        data["project"]["source"]["file"]["ast"]["components"] || [],
        fn
          %{"transforms" => transforms} when not is_nil(transforms) ->
            Enum.map(transforms, fn %{"transformed" => transformed} ->
              if is_nil(transformed) do
                nil
              else
                Jason.decode!(transformed)
              end
            end)

          _ ->
            []
        end
      )

    ast =
      data["project"]["source"]["file"]["ast"]["tree"]
      |> Jason.decode!()
      |> Traverse.mapall(fn
        %{"loc" => loc} = ast ->
          Enum.find(transforms_, fn %{"loc" => loc_} ->
            loc === loc_
          end)
          |> case do
            nil ->
              ast

            transform ->
              transform
          end

        x ->
          x
      end)
      |> Jason.encode!()

    {:ok, %Rambo{status: _status, out: ast_, err: _err}} =
      Rambo.run(Application.app_dir(:stasis, "priv/ports/astring"), in: ast)

    ast_
  end
end
