defmodule Stasis.Using.Subscribable do
  alias Stasis.Utils.RegistrationName, as: Reg

  defmacro __using__(module) do
    quote do
      def subscribe(project) do
        Phoenix.PubSub.subscribe(
          Stasis.PubSub,
          Reg.instance(project, unquote(module))
        )
      end

      def broadcast(project, message) do
        Phoenix.PubSub.broadcast(
          Stasis.PubSub,
          Reg.instance(project, unquote(module)),
          Tuple.append(message, project)
        )
      end
    end
  end
end
