defmodule Stasis.Using.GetSetKey do
  alias Stasis.Utils.RegistrationName, as: Reg

  defmacro __using__(module) do
    quote do
      def handle_call({:getkey, key}, _from, state) do
        {:reply, {:ok, Keyword.get(state, key, nil)}, state}
      end

      def handle_call({:setkey, key, value}, _from, state) do
        try do
          {:reply, {:ok, value}, Keyword.replace!(state, key, value)}
        rescue
          _ -> {:reply, {:error, "Cannot Set Key: #{key}"}, state}
        end
      end

      def getkey(project, key) do
        case GenServer.whereis(Reg.via(project, unquote(module))) do
          nil -> {:error, "Server Not Found: #{project}"}
          pid -> GenServer.call(pid, {:getkey, key})
        end
      end

      def setkey(project, key, value) do
        case GenServer.whereis(Reg.via(project, unquote(module))) do
          nil -> {:error, "Server Not Found: #{project}"}
          pid -> GenServer.call(pid, {:setkey, key, value})
        end
      end
    end
  end
end
