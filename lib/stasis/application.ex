defmodule Stasis.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      StasisWeb.Telemetry,
      {Phoenix.PubSub, name: Stasis.PubSub},
      Stasis.Project.Dynamic,
      StasisWeb.Endpoint,
      {Absinthe.Subscription, StasisWeb.Endpoint},
      {Registry, keys: :unique, name: Stasis.Registry}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Stasis.Supervisor)
  end

  def config_change(changed, _new, removed) do
    StasisWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
