defmodule Stasis.Transform.Condition do
  use TypedStruct

  alias __MODULE__, as: Con

  typedstruct do
    @typedoc "AST Transform Condition"

    field(:type, :component | nil)
    field(:key, String.t() | nil)
    field(:value, String.t() | nil)
  end

  def component(ast, %Con{key: key}) do
    Stasis.Ast.ReactCreateElement.find(ast, key)
  end
end
