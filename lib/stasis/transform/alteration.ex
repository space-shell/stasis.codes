defmodule Stasis.Transform.Alteration do
  use TypedStruct

  alias ESTree.Tools.Builder, as: B

  alias __MODULE__, as: Alt

  typedstruct do
    @typedoc "AST Transform Alteration"

    field(:type, :component | nil)
    field(:key, String.t() | nil)
    field(:value, String.t() | nil)
  end

  def component(ast, %Alt{type: type, value: value}) do
    IO.inspect(ast)

    case type do
      "render" ->
        case value do
          "true" -> nil
          "false" ->
            B.literal("div", nil, ast["loc"])
            |> Map.from_struct()
            |> Jason.encode!()
        end
      _ -> nil
    end
  end
end
