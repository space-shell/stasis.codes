defmodule Stasis.Graph.SchemaTypes do
  use Absinthe.Schema.Notation

  object :project do
    field(:name, :string) do
      resolve(fn project, _args, _context ->
        {:ok, project}
      end)
    end

    field(:source, :source) do
      resolve(fn project, _args, _context ->
        {:ok, project}
      end)
    end
  end

  object :source do
    field(:url, :string) do
      resolve(fn project, _args, _context ->
        Stasis.Project.SourceFile.getkey(project, :url)
      end)
    end

    field(:file, :file) do
      resolve(fn project, _args, _context ->
        {:ok, project}
      end)
    end
  end

  object :file do
    field(:content, :string) do
      resolve(fn project, _args, _context ->
        Stasis.Project.SourceFile.getkey(project, :source)
      end)
    end

    field(:hash, :string) do
      resolve(fn project, _args, _context ->
        case Stasis.Project.SourceFile.getkey(project, :source) do
          {:ok, nil} ->
            {:ok, nil}

          {:ok, file} ->
            {:ok, :crypto.hash(:sha256, file) |> Base.encode16()}
        end
      end)
    end

    field(:ast, :ast) do
      resolve(fn project, _args, _context ->
        case Stasis.Project.SourceFile.getkey(project, :ast) do
          {:ok, nil} ->
            {:ok, nil}

          {:ok, ast} ->
            {:ok, ast}
        end
      end)
    end
  end

  object :ast do
    field(:tree, :string) do
      resolve(fn tree, _args, _context ->
        {:ok, tree}
      end)
    end

    field(:components, list_of(:component)) do
      resolve(fn tree, _args, _context ->
        tree
        |> Jason.decode!()
        |> Stasis.Ast.ReactCreateElement.list()
        |> (&{:ok, &1}).()
      end)
    end

    field(:component, :component) do
      arg(:name, :string)

      resolve(fn tree, %{name: name}, _context ->
        tree
        |> Jason.decode!()
        |> Stasis.Ast.ReactCreateElement.exists(name)
        |> if do
          name
        else
          nil
        end
        |> (&{:ok, &1}).()
      end)
    end
  end

  object :component do
    field(:name, :string) do
      resolve(fn name, _args, _context ->
        {:ok, name}
      end)
    end

    field(:transforms, list_of(:transform)) do
      resolve(fn name, _args, _context ->
        Stasis.Project.Transforms.read("google", "component", name)
      end)
    end

    field(:transform, :transform) do
      arg(:id, :id)

      resolve(fn name, %{id: id}, _context ->
        Stasis.Project.Transforms.read("google", "component", name, id)
      end)
    end
  end

  object :transform do
    field(:id, :id)
    field(:active, :boolean)

    field(:transformed, :string) do
      resolve(fn %Stasis.Project.Transforms{active: active, condition: condition, alteration: alteration},
                 _args,
                 %{context: %{project: project}} ->
        if active do
          case Stasis.Project.SourceFile.getkey(project, :ast) do
            {:ok, nil} ->
              nil

            {:ok, ast} ->
              ast
              |> Jason.decode!()
              |> Stasis.Transform.Condition.component(condition)
              |> case do
                nil ->
                  nil
                component ->
                  component
                  |> Stasis.Transform.Alteration.component(alteration)
              end
          end
        else
          nil
        end
        |> (&{:ok, &1}).()
      end)
    end

    field(:condition, :condition)
    field(:alteration, :alteration)
  end

  object :condition do
    field(:type, :string)
    field(:key, :string)
    field(:value, :string)
  end

  object :alteration do
    field(:type, :string)
    field(:key, :string)
    field(:value, :string)
  end
end

defmodule Stasis.Graph.Schema do
  use Absinthe.Schema
  use OK.Pipe

  alias Stasis.Utils.RegistrationName, as: Reg

  import_types(Stasis.Graph.SchemaTypes)

  subscription do
    field :source_url, :string do
      config(fn _args, %{context: %{project: project}} ->
        {:ok, topic: Reg.instance(project, ["source_url", "mutate"])}
      end)
    end

    field :components, list_of(:component) do
      config(fn _args, %{context: %{project: project}} ->
        {:ok, topic: Reg.instance(project, ["components", "mutate"])}
      end)
    end

    field :projects, list_of(:string) do
      config(fn _args, %{context: %{project: project}} ->
        {:ok, topic: Reg.instance(project, ["projects", "mutate"])}
      end)
    end

    field :transform_component, :transform do
      config(fn _args, %{context: %{project: project}} ->
        {:ok, topic: Reg.instance(project, ["transform", "component", "mutate"])}
      end)
    end
  end

  mutation do
    @desc "Update Source File URL"

    field :source_url, type: :string do
      arg(:url, :string)

      resolve(fn _, %{url: url}, %{context: %{project: project}} ->
        Stasis.Project.SourceFile.setkey(project, :url, url)
        ~> tap(&Stasis.Project.SourceFile.get_source_file(&1, project))
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            transform_component: Reg.instance(project, ["source_url", "mutate"])
          )
        )
      end)
    end

    field :transform_add, type: :transform do
      arg(:category, non_null(:string))
      arg(:name, non_null(:string))

      resolve(fn _, %{category: category, name: name}, %{context: %{project: project}} ->
        Stasis.Project.Transforms.create(project, category, name)
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            source_url: Reg.instance(project, ["transform", "component", "mutate"])
          )
        )
      end)
    end

    field :transform_active_toggle, type: :transform do
      arg(:category, non_null(:string))
      arg(:name, non_null(:string))
      arg(:id, non_null(:id))

      resolve(fn _, %{category: category, name: name, id: id}, %{context: %{project: project}} ->
        Stasis.Project.Transforms.update(
          project,
          category,
          name,
          id,
          fn transform ->
            Map.update(transform, :active, false, &(not &1))
          end
        )
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            source_url: Reg.instance(project, ["transform", "component", "mutate"])
          )
        )
      end)
    end

    field :transform_delete, type: :transform do
      arg(:category, non_null(:string))
      arg(:name, non_null(:string))
      arg(:id, non_null(:id))

      resolve(fn _, %{category: category, name: name, id: id}, %{context: %{project: project}} ->
        Stasis.Project.Transforms.delete(project, category, name, id)
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            source_url: Reg.instance(project, ["transform", "component", "mutate"])
          )
        )
      end)
    end

    field :transform_alteration_update, type: :transform do
      arg(:category, non_null(:string))
      arg(:name, non_null(:string))
      arg(:id, non_null(:id))
      arg(:type, :string)
      arg(:key, :string)
      arg(:value, :string)

      resolve(fn _,
                 %{
                   category: category,
                   name: name,
                   id: id,
                   type: type,
                   key: key,
                   value: value
                 },
                 %{context: %{project: project}} ->
        Stasis.Project.Transforms.update(
          project,
          category,
          name,
          id,
          &Map.put(&1, :alteration, %Stasis.Transform.Alteration{
            :type => type,
            :key => key,
            :value => value
          })
        )
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            source_url: Reg.instance(project, ["transform", "component", "mutate"])
          )
        )
      end)
    end

    field :transform_condition_update, type: :transform do
      arg(:category, non_null(:string))
      arg(:name, non_null(:string))
      arg(:id, non_null(:id))
      arg(:type, :string)
      arg(:key, :string)
      arg(:value, :string)

      resolve(fn _,
                 %{
                   category: category,
                   name: name,
                   id: id,
                   type: type,
                   key: key,
                   value: value
                 },
                 %{context: %{project: project}} ->
        Stasis.Project.Transforms.update(
          project,
          category,
          name,
          id,
          &Map.put(&1, :condition, %Stasis.Transform.Condition{
            :type => type,
            :key => key,
            :value => value
          })
        )
        ~> tap(
          &Absinthe.Subscription.publish(StasisWeb.Endpoint, &1,
            source_url: Reg.instance(project, ["transform", "component", "mutate"])
          )
        )
      end)
    end
  end

  query do
    @desc "Project Information"

    field(:projects, type: non_null(list_of(:project))) do
      resolve(fn _, _args, _context ->
        Stasis.Project.Dynamic.projects()
        |> (&{:ok, &1}).()
      end)
    end

    field(:project, type: :project) do
      resolve(fn _root, _args, %{context: %{project: project}} ->
        case Stasis.Project.Dynamic.project(project) do
          nil ->
            {:error, "No Project Found"}

          project ->
            {:ok, project}
        end
      end)
    end
  end
end
