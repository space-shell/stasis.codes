defmodule Stasis.Utils.RegistrationName do
  @name_seperator ":"

  def children(project, modules) do
    project
    |> via(modules)
    |> GenServer.whereis()
    |> case do
      nil -> []
      pid -> Supervisor.which_children(pid)
    end
    |> Enum.reduce(%{}, fn {_, pid, _, _}, acc ->
      pid
      |> (&Registry.keys(Stasis.Registry, &1)).()
      |> Enum.map(&String.split(&1, @name_seperator))
      |> Enum.filter(&Enum.member?(&1, project))
      |> Enum.reduce(%{}, fn key, acc ->
        Map.put(acc, Enum.join(key, @name_seperator), pid)
      end)
      |> Map.merge(acc)
    end)
  end

  def name(project, modules) do
    {:name, via(project, modules)}
  end

  def via(project, modules) do
    instance(project, modules)
    |> (&{:via, Registry, {Stasis.Registry, &1}}).()
  end

  def instance(project, modules) when is_atom(modules) do
    modules
    |> Atom.to_string()
    |> (&instance(project, &1)).()
  end

  def instance(project, modules) when is_bitstring(modules) do
    modules
    |> String.split(".")
    |> Enum.reject(&(&1 in ["Elixir", "Stasis"]))
    |> Enum.map(&String.downcase/1)
    |> (&instance(project, &1)).()
  end

  def instance(project, modules) when is_list(modules) do
    [project]
    |> Enum.concat(modules)
    |> Enum.join(@name_seperator)
  end

  def project() do
    Stasis.Registry
    |> Registry.keys(self())
    |> tap(&IO.inspect/1)
    |> hd()
    |> String.split(@name_seperator)
    |> hd()
  end

  def call(args, module, project \\ project()) do
    case GenServer.whereis(via(project, module)) do
      nil -> {:error, "Project is not online"}
      pid -> GenServer.call(pid, args)
    end
  end
end
