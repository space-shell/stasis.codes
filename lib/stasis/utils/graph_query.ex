defmodule Stasis.Utils.GraphQuery do
  alias Stasis.Utils.RegistrationName, as: Reg

  def runner(project) do
    fn query ->
      run(query, project)
    end
  end

  def run(query, project \\ Reg.project()) do
    Absinthe.run(
      query,
      Stasis.Graph.Schema,
      context: %{pubsub: StasisWeb.Endpoint, project: project}
    )
  end

  def subscribe(topic) do
    StasisWeb.Endpoint.subscribe(topic)
  end
end
