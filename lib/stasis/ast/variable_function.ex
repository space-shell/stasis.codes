defmodule Stasis.Ast.VariableFunction do
  def resolve(ast) do
    ast_ = Jason.decode!(ast)

    # TODO - Change list to Set
    Traverse.walk(ast_, [], fn
      %{
        "type" => "FunctionDecleration",
        "id" => %{
          "type" => "Identifier",
          "name" => name
        }
      },
      acc ->
        [name | acc]

      %{
        "type" => "VariableDeclaration",
        "declarations" => [
          %{
            "type" => "VariableDeclarator",
            "id" => %{
              "type" => "Identifier",
              "name" => name
            },
            "init" => %{
              "type" => "FunctionExpression",
              "params" => params
            }
          }
        ]
      },
      acc ->
        params
        |> Enum.reduce([], fn
          %{"type" => "Identifier", "name" => name_param}, a ->
            [name_param | a]

          _, a ->
            a
        end)
        |> (&{name, &1}).()
        |> (&[&1 | acc]).()

      _, acc ->
        acc
    end)
    |> Enum.uniq()
  end
end
