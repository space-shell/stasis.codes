defmodule Stasis.Ast.ReactCreateElement do
  def exists(ast, name) do
    Traverse.walk(ast, false, fn
      %{
        "callee" => %{"property" => %{"name" => "createElement"}},
        "arguments" => [%{"type" => "Identifier", "name" => name_} | _rest]
      },
      acc
      when not acc and name_ === name ->
        true

      _, acc ->
        acc
    end)
  end

  def find(ast, name) do
    Traverse.walk(ast, nil, fn
      %{
        "callee" => %{"property" => %{"name" => "createElement"}},
        "arguments" => [%{"type" => "Identifier", "name" => name_} = element | _rest]
      },
      acc
      when is_nil(acc) and name_ === name ->
        element

      _, acc ->
        acc
    end)
  end

  def list(ast) do
    # TODO - Change list to Set
    Traverse.walk(ast, [], fn
      %{
        "callee" => %{"property" => %{"name" => "createElement"}},
        "arguments" => [%{"type" => "Identifier", "name" => name_} | _rest]
      },
      acc ->
        [name_ | acc]

      _, acc ->
        acc
    end)
    |> Enum.uniq()
  end
end
